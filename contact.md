# How to Contact Me

The best way to reach me is by email.

* Send friendly messages to [matthew (at) matthewgraybosch.com](mailto:matthew@matthewgraybosch.com).
* Send confidential messages to [matthewgraybosch (at) tutanota.com](mailto:matthewgraybosch@tutanota.com).

I'm serious. I actually miss getting email from human beings, so if
you want to drop me a line you're welcome to do so.

## Instant Messages

If you prefer instant messaging, you can reach me via
[Wire](https://wire.com). I'm @matthewgraybosch on that service.

## GitLab

I have a [GitLab profile](https://gitlab.com/mgraybosch) since I use the 
service to host git repositories for my websites and my 
[Starbreaker](https://starbreakersaga.com) project.

## Social Media

While I have social media accounts, I maintain them mainly because
having them and not using them is less of a pain in the ass than not
having them.

* Twitter: [@MJGraybosch](https://twitter.com/MJGraybosch)
* Facebook: [Matthew Graybosch](https://facebook.com/matthew.graybosch10)

If you absolutely *must* hit me up via social media, your best bet is
to join a small Fediverse server and @ one of my accounts.

* [@starbreaker@octodon.social](https://octodon.social/@starbreaker)
* [@starbreaker@pleroma.site](https://pleroma.site/users/starbreaker)
