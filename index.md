# Welcome

Hello. I'm Matthew Graybosch and this is my website. I can't offer
much in the way of hospitality since this isn't a real place, but feel
free to poke around and let me know you were here.

## Posts

* [Castlevania Season 2](/blog/2018/castlevania-season-2.html "2018-10-26 20:01 EST")
* [CompTIA Linux+ Certification](/blog/2018/comptia-linux-certification.html ""2018-10-25 16:40 EST")
* [Google+ Could Have Been a Contender](/blog/2018/google-plus-contender.html "2018-10-08 22:58 EST")
* [Turning Forty](/blog/2018/turning-forty.html "2018-09-24 22:00 EST")
* [Don't Panic](/blog/2018/dont-panic.html "2018-09-05 20:00 EST")

For older posts, please visit the [archive](/blog/).

## Pages

* [About](/about.html) &mdash; more about me
* [Resume](/resume.html) &mdash; what I do at my day job
* [Contact](/contact.html) &mdash; how to reach me
* [Elsewhere](/elsewhere.html) &mdash; where else you can find me online
* [Subscribe](/subscribe.html) &mdash; how to get updates
* [Site Map](/sitemap.html) &mdash; every page on this site
* [Site Info](/colophon.html) &mdash; implementation details
