# About Me

I’m Matthew Graybosch, a long-haired metalhead from New York exiled to
the wilds of central Pennsylvania for my sins. I’m the author of the
science fantasy novels *Without Bloodshed* and *Silent Clarion*. I'm
currently developing a novella called *When You Don't See Me* and a
novel called *Dissident Aggressor*. If any of that sounds interesting,
take a look at [starbreakersaga.com](https://starbreakersaga.com).

I’m also a self-taught software developer who works mainly with
Microsoft tech on the job. I currently work for Deloitte Consulting at
my day job, but *everything* I post online is strictly my own opinion
and not representative of my employer or their views.

When I'm not working for a living, I tinker with Unix-based operating
systems because I like having the sort of computing power once
reserved to universities and large corporations at my fingertips. I
use free and open source software for all of my personal computing
needs because I honestly prefer to do so. I build my own websites, and
often provide *pro bono* advice on how to design and build responsive
and accessible websites.

If you want to, you can verify my online identity using
[Keybase](https://keybase.io/demifiend).

[![Matthew Graybosch atop Rockefeller Center in Manhattan](/images/author-nyc.png)](/images/author-nyc.jpg)
