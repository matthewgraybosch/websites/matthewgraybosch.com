# Turning Forty

Today is my fortieth birthday. I've spent it at work thus far, taking
stock of my life and the world around me. Life didn't turn out the way
I hoped it would, but you've heard that story before. Chances are it's
your story as well.

Why am I at work? The simple answer is sheer necessity; I didn't have
the foresight to marry a rich woman -- or be born to rich parents --
so I must perforce work for a living. Of course, this doesn't
necessarily preclude me taking my birthday *off*, which I might have
done if I were a sensible person.

However, I didn't do the sensible thing because I'm an adult, I have
responsibilities, and people are counting on me to do the work I
agreed to do when I agreed to do it. This is part of the compromise I
made with reality as a young man: I accepted that I would have to earn
a living *somehow* while I worked on my writing.

But I'm hardly special. I suspect most people spend their fortieth
birthdays at work, either out of necessity or for lack of anything
better to do. 

## Only Myself to Blame

If it sounds like I'm not content with my circumstances, it's because
I'm not. I'm not the writer I want to be. I'm not the person I hoped I
would become. However, most of that is my own damn fault.

* I chose to live with depression instead of fighting harder to get
  help.
* I chose, again and again, to not reach out to other people and at
  least *try* to make friends.
* I chose to put aside music.
* I chose to go to college right after high school.
* I chose to half-ass my way through college once I got there.
* I chose to take that first coding job in Connecticut.
* I chose to stay at that job despite my boss being an incorrigible
bully.
* I chose to move to Pennsylvania after rage-quitting the job I had in
  Connecticut after nine years.
* I chose to keep taking software development jobs even though I was
  burned out and sick of coding for a living.
* I chose to publish with a small press instead of biding my time,
  continuing to rewrite *Starbreaker*, and either going full indie or
  trying for a deal with a major publisher.

## What The Hell Was I Thinking

Seeing these choices laid out, it's tempting to think they're just a
cascade of boneheaded decisions. Maybe they are, but I had reasons for
most of them.

* Mental health professionals couldn't help my mother with *her*
  depression, and they didn't even bother to try helping me. Instead,
  they'd just write a prescription for drugs that didn't help.
* I managed reasonably well without friends as a child and a teenager,
  so why change?
* I didn't know how I could go about earning a living as a musician,
  and didn't believe that I could in any case.
* I might not have wanted to go to college, but it was either that,
  enlist in the military, or move out and get a full time job right
  away. I had to do *something*. It never occurred to me to consider
  trade school, and my parents probably would have had kittens at the
  notion of their "genius" son becoming an electrician or a machinist.
* I should have done more with my time in college, but I was also
  working part-time, commuting to school instead of living on-campus,
  had no friends or support, and honestly did not want to be there.
* That first job came in 2000, and seemed like a lucky break at the
  time because it came from a distant relation during the recession
  that had occurred after the bursting of the dot-com bubble.
* By the time I realized that my boss was a bully and the only way to
  get away from her was to quit, it was too late to make an easy
  escape. I had parents, a girlfriend (and her family) to whom I
  wanted to prove that I was a solid, reliable adult. Besides, "real
  men" didn't quit just because they were unhappy.
* There wasn't much in the way of software development work in
  Connecticut, and while I probably could have found work in New York
  City, C. and I were sick of living in CT. Also, my parents were
  making noises about wanting us to live closer to them as they got
  older.
* I kept looking for software development jobs for two simple reasons:
  I had experience as a developer, and I lacked the time, money, and
  inclination to go back to college and get a degree that *might*
  allow me to do something else for a living.
* Publishing with Curiosity Quills Press seemed like a golden
  opportunity at the time, a shortcut around the bullshit that
  traditional publishing entails (query an agent, hope the agent can
  find you a publisher, etc.) and a way to avoid the bullshit
  independent publishing entails (do your own marketing, even though
  nobody cares about you or your work).

## Just Making Excuses For Myself

All of these are rationalizations after the fact, of course. I can't
honestly say I thought through all of my decisions, but most people
don't. We're not rational, but rationalizing. We make our decisions on
the basis of impulse, emotion, or intuition and come up with logical
reasons for why we decided as we did later on. I'm hardly unique in
this regard. [Motivated
reasoning](https://www.psychologytoday.com/us/basics/motivated-reasoning
"Psychology Today: Motivated Reasoning") is one of the flaws that make
us human.

I'm just using the occasion of my fortieth birthday as an opportunity
to take stock of the life I've made for myself and make peace with my
choices. I don't want to be one of those middle-aged men who make an
utter shambles of their lives in their forties because they are no
longer able to repress their dissatisfaction with their lives and do
irrational and drastic things like quitting their jobs, buying
expensive vehicles or spending time with a new and younger lovers
without at least discussing it with their spouses first.

I might not be completely happy with my life, but a flashy new car or
a younger lover isn't going to fix that. Besides, I've done reasonably
well for myself compared to many people my age:

* I have a steady job with respectable benefits
* I have my own house
* I can afford to pay extra on the mortgage's principal every month
* I own my car outright
* I'm not mired in credit card debt
* I'm not constantly fighting with my partner over meaningless bullshit
* I can honestly say that my partner and I still love each other
* I still have most of my hair
* I haven't been diagnosed with anything scary yet
* I have written three novels and published two of them.

The last is especially important. Thousands of people set out to write
novels. Most never finish their first draft. Of those who finish a
draft, few ever refine it enough to have something that somebody else
is willing to publish. I managed to do it twice, and I think I've
earned the right to a little pride.

## Welcome to my Midlife Crisis

Why would I throw away what I've got after my partner and I worked so
hard to get where we are today? If Hollywood is any guide, that's
"what men of a certain age do."

It's the [midlife
crisis](http://www.zocalopublicsquare.org/2018/06/01/feminists-invented-male-midlife-crisis/ideas/essay/),
but does this really only happen to men? Or does our culture simply
encourage men to do this?

It's an interesting question, but only insofar as it prompts me to ask
a different question: what kind of man have I become? Do I even think
of myself as a man? I certainly don't think of myself as a woman; I am
not transgender.

## A Miserable Little Pile of Secrets

However, being a man was something that never quite felt right to
me. I was never especially masculine, at least not according to
prevailing norms, and I only made half-hearted attempts at "being a
man" because it didn't occur to me that I had the ability -- or the
right -- to reject gender altogether.

What is a man, anyway? "A miserable little pile of secrets," according
to both [Andre
Malraux](https://en.wikiquote.org/wiki/Andr%C3%A9_Malraux 
"Wikiquote: Andre Malreaux") and [Count
Dracula](https://knowyourmeme.com/memes/what-is-a-man 
"Know Your Meme: What is a Man").

Am I a man simply because I was born with XY chromosomes, a penis, and
a pair of testicles? If so, then I was conscripted into masculinity and am
under no obligation to perform my gender. If being a man is something
one must choose, then I still want no part of it; mere masculinity is
too confining.

Indeed, why should any of us settle for a social role that demands we
amplify some qualities and repress others? Why should any of us be
men or women?

In the absence of an answer, I've decided upon my own: I shall figure
out how to be more truly myself in the years left to me. And if I
trust you enough to put aside the persona and show you who I am, be
grateful, for you'll be one of a privileged few.

## I'm Forty, and I Like It

Now that I'm forty years old, I no longer feel obligated to prove
myself to others. I'm not interested in conforming to your
expectations of what sort of person I should be. I spent my childhood,
youth, and my adult years thus far trying and often failing to measure
up. No more.

It's *my* time now. It's time I focused on what *I* want. More
importantly, it's time I understood my own needs and learned to center
them.

Hopefully you'll do the same. Ideally, you're reading this while you're
still a teenager or young adult yourself, and can thus avoid making
some of the same mistakes I did. Otherwise, I hope you find something
of value.

Either way, feel free to drop me an email.
