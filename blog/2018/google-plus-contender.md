# Google+ Could Have Been a Contender

It's all over the tech news. I'll link to [Ars
Technica's](https://arstechnica.com/tech-policy/2018/10/google-exposed-non-public-data-for-500k-users-then-kept-it-quiet/)
article, but any outlet you pick is going to link to and quote the
*Wall Street Journal*, which reported on Monday that Google leaked
personal data for over 500,000 Google+ users between 2015 and
March 2018. They fixed the leak back in March, and then *hid it* until
now to avoid having a regulatory hammer dropped on them.

This goes *all* the way to the top, according to the WSJ. CEO Sundar
Pinchai himself knew about the breach and the cover-up. According to
randos on Hacker News, the WSJ's coverage is part of a long-running
beef between Google and the conservative establishment.

I don't get paid enough to care either way, so ordinarily I'd be
content to make some popcorn and enjoy the *schadenfreude*. But
there's one little complication.

You see, [I've been on Google+ since the beta in
2011](https://plus.google.com/+MatthewGraybosch). I might not have
written a single line of code, but every post I shared and every
comment I submitted on other people's posts represents dozens --
perhaps *hundreds* -- of unpaid hours I spent making Google+ something
more than just some Googler's Friday afternoon side project. I
expended all of that effort, as did tens of thousands of others.

Google laid the foundation, but we made something out of Google+. We
made it something with which neither Facebook nor Twitter could
compete. We made it something tech journalists didn't
understand. Twitter was and is the bathroom wall of the
internet. Facebook started out as a never-ending high school reunion,
and became everybody's weird aunts and uncles making a cacophony of
craziness.

I remember what it was like. Techies clamored for beta invites like
they were [Golden Tickets in a Wonka
bar](https://en.wikipedia.org/wiki/Charlie_and_the_Chocolate_Factory). We
invited people we thought might be interesting, and we talked about
things that interested us.

We made art and shared it. We made music and shared it. We wrote
stories and shared them. People got jobs because of Google+. They made
friends. They found love. They found publishers and fans. When Occupy
Wall Street went down, we watched and did what we could to help from a
distance. President Obama used Google Hangouts to talk to us.

**Google+ could have been a contender.** It could have been the social
network for smart, creative people -- the social network for authors,
artists, hackers, scientists, academics, etc.

But Google screwed it up.

* They did a half-assed job with their public API. It was useless
  compared to what Twitter and Facebook were offering.
* They promised that you could embed Google+ in your blog for seamless
  sharing and commenting, but only delivered such functionality for
  Blogger users.
* They tried to force everybody to use their "real names", by which
  they meant the name on your government-issued ID.
* They tried to compete with Facebook head-on, not realizing that the
  people they were attracting to Google+ were the very people for whom
  Facebook had no real appeal.
* They tried to forcibly integrate Google+ with YouTube by making
  everybody on YouTube have a Google+ account. Now, if you've ever
  tried reading the comments on the average YouTube video, you would
  not need me to explain why this was a terrible idea.
* They began privileging pictures and video over text, even though
  they had a social network where people would regularly belt out
  five-hundred word essays as *comments* on somebody else's
  three-thousand word post.
  
They did *all* of this, and by the time the clock ticked over to 2014
the damage was done. Google+ was a shell of what it could have
been. Many of us tried to keep the faith. We surveyed the wreckage,
realized that Google no longer cared about us, and resolved to keep
using Google+ our way as long as Google was content to keep the
servers running.

But our revels now are ended. If Google needed an excuse to shut down
Google+, this breach will surely do. Now we have ten months to bid
farewell to the network we built with our posts, comments, and
reshares when we could have been building our own blogs on the open
Web.

Now we have ten months to find a new home on the network. Hopefully
we'll find one where we can more firmly control our own destiny,
instead of hoping that a corporation's interests will align with our
own. Lacking that, maybe we'll find ways to help those without domains
and websites of their own become full citizens of the network.

Now, while I wrote this, I had two songs playing. In one, Jefferson
Starship told me "we built this city on rock 'n roll". In another, the
Protomen told me, "don't turn your back on the city".

Google+ isn't the city. It's just a bunch of buildings on a plot of
land. We the people are the city. We made Google+ what it was in its
heyday. We kept the flame alive when Google tried to take what we made
and turn it into the weapon with which it would strike down
Facebook. And even when Google left Google+ in ruins, we remained.

Thanks for the memories, you guys. I'll see you around the network. I
don't know where yet. Maybe I'll see you on
[Pluspora](https://pluspora.com/people/75778ca0ad5601364692005056268def). Maybe
I'll see you on [Mastodon](https://octodon.social/@starbreaker). Maybe
I'll see you on the open web or get an email from you. But know this:
I mean to spend the next ten months going through my circles. I'm
going to compile whatever contact information I can from your
profiles, and I'm going to do what I can to make sure we don't lose
touch.

I won't turn my back on the city.

![Artwork of a motorcyclist riding parallel to a distant city
skyline](/images/breaking_out_by_angeloolson-d7ekkvo.jpg)

Artwork by [Angelo
Olson](https://www.deviantart.com/angeloolson/art/Breaking-Out-447738036)
