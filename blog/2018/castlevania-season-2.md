# Castlevania Season 2

Dracula's lack of interest in the implementation details of his
objective to purge the world of humanity is every manager and CEO
ever. "Don't bother me with the details. Just get it done."
