# Don't Panic

If you're visiting this site after my latest update, you're probably
wondering where everything went. Likewise if your search engine
directed you to a particular post and you got a [404](/404.html)
instead of the article, outtake, or rant you expected.

Don't panic. I haven't been hacked. I still have all of that old
material.

But I've chosen to strip my website down to bare bones, and pull down
all of my old content. I'm turning forty this month, and a brand new
website without any need to migrate old content is the closest a man
like me can get to a fresh start without changing jobs, getting a
divorce, and buying an expensive sports car.

This will eventually work out in your favor as well. On the old
website, I was trying to post everything that mattered to me on the
same blog, whether it was about my
[Starbreaker](https://starbreakersaga.com) science fantasy project,
OpenBSD as a desktop OS, a new metal or synthwave album I discovered,
pseudo-intellectual ramblings, or just rants about current events.

All of that will go to [separate websites](/elsewhere.html) now. This
main website will be a chill, low key landing site with occasional
updates where I can present a business casual face to the world.

For anybody interested, I've build this new site using a [customized
version](/ssg) of a shell script [Roman
Zolotarev](https://www.romanzolotarev.com/ssg.html) created for his
own use and was generous enough to offer to the public.

If you have any questions or comments, please [email
me](mailto:matthew@matthewgraybosch.com) or hit me up [on
Mastodon](https://octodon.social@starbreaker).
