# CompTIA Linux+ Certification

I finally got my study guide for the CompTIA Linux+ certification
exams last night after ordering it two weeks ago. The problem is that
I took the test on Monday. Thanks for nothing, FedEx.

I should have done reasonably well regardless, though the test did
cover situations and applications that I don't normally encounter at
home.

It also had questions biased toward CentOS/Red Hat, but that's what I
get for using mostly Debian and friends before switching to
OpenBSD. Too bad I'm out a hundred bucks for a study guide that didn't
do me any good if I end up passing the exam.

[discussion on diaspora](https://pluspora.com/posts/c843a5e0ba990136b86300505608f9fe)
