==================================================================
https://keybase.io/demifiend
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://matthewgraybosch.com
    * I am demifiend (https://keybase.io/demifiend) on keybase.
      * I have a public key with fingerprint C415 943A 15A2 8DC4 472F
    * DCD0 8179 6380 E895 DA9F

To do so, I am signing this object:

{
  "body": {
      "key": {
            "eldest_kid":
  "0101b5250bd2acced976bc2f6b41c1685dded82914898c6949cc553cb6dd1a2d8eb60a",
        "fingerprint": "c415943a15a28dc4472fdcd081796380e895da9f",
	      "host": "keybase.io",
	            "key_id": "81796380e895da9f",
		          "kid":
  "0101b5250bd2acced976bc2f6b41c1685dded82914898c6949cc553cb6dd1a2d8eb60a",
        "uid": "75d9b03d5c525f143750ee732efc5219",
	      "username": "demifiend"
	          
},
    "service": {
          "hostname": "matthewgraybosch.com",
	        "protocol": "https:"
		    
},
    "type": "web_service_binding",
        "version": 1
	  
},
  "ctime": 1536341916,
    "expire_in": 157680000,
      "prev":
  "128ae8fd80ffeb791d4657769ac4e257c2e9cea14e22f727e3b683dde0bc0755",
    "seqno": 18,
      "tag": "signature"
      
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.77
Comment: https://keybase.io/crypto

yMIzAnicrZJbSBRRGMd3rSyV7E4XhWIIi1y2OTNz5rIRXbCohyUpulDZNucyu0O7
M9vuaFlJRZSRaJFhrG2RYpAUgWFadiG3ojt0eegKhVQQJC1FQVDRGbG3Hjsvh/Px
///O//v4bo4c4sn3Lv7cTnsQ7ffe722p9Kw73J3eziGbVHOB7dwmOnDRKKFJJ7TJ
JFyA4wEPEBQgj4igY0yJpsgIC4aMJICBrEJCKFEFDUiqpmJZkzSMIRQxkgkBukBU
imRe53ycYVphmognTMthWCwBqEmiDqAuqARLkiIYBBNeBYomiypPVQ0SXTOYMWIn
XQcLh/Qk9Zs2q7FHaCDeP/T/OXflAE6BREO8SCBmTANIogJ5ShVRoAarAM0VJmnC
0mOUqQmNmYZJLcLV+DhWrjIxdSfrtjIoiemOE6Fbwgm9GtlJHPFjO8Yg8YTt2NiO
MkXEceLJgEtwquOuZQtFoUFYCJkWYQNljiqaSJq2xQUAU2LHdOkAirIoAQ3IPo5u
jZsJGjJdBVRklWfH/YdWMSQQVJ2qBlF5w6BI0QCRZKgosqZjiQpQwQLVMNUBewiG
IihURLIqstHxCPMKhJzb3mbLZmyV5dTDjJk0w5buVCYoV5O5vn6ox5vvyR2W426a
Jz9v9N/9m395hCdlzswJTRrXfCM8p3R3x+TcctyRefKjadzV8c+DJe2vYw13HgeD
P/MeLIjfq98dDNyeYsK2ORvbn+3YNbbVzkz4XSTE+rxlgJz78OlgV+m2FaGzrdHh
3bfWFw0Ragve7JnVlErP+LUyixbum/bt3tcNJ6aOWjKq98Lcd2Vrotqr3P6eMSuO
Xxmaqnt+7OXs4l1qxSd/y9eny490Pdrh4Rv6i5Z+v+abWbzs0IGdn+t+FZTX1Td/
OCl1fOzMFJznn6zmJe7Ml6aL/ulHoxU18y4uaCsJ7+1p7MwuOv0wJRb2rVnbfe5d
bV520pT3hY0RfHfPJU9of9eqt33ZkS/epNuK955KT2z+A6ItRWQ=
=ZIDU
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/demifiend

==================================================================
