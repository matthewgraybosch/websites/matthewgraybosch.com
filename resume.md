# My Resume

This isn't a formal resume, nor is it intended as one. I will provide
one on request when I am available for hire. See "Availability" below
for details.

## Value Proposition

I'm not a rock star, a ninja, or a 10x developer. I'm the guy they
*all* depend on. I'm the guy who makes your MVP most valuable by doing
the boring or dirty work they lack the time or inclination to do
themselves.

## What I Do

I'm a jack of all trades, and a master of a few. My strongest skills
are C#, .NET, SQL Server, but I can do just about anything you might
need a developer to do. I can even wear the DBA or system
administrator hats if you don't have specialists handy. I pick up new
languages, frameworks, and technologies as needed while familiarizing
myself with your existing code base.

I run OpenBSD at home. 1998 was *my* Year of the Linux Desktop. I
build my own websites using customized shell scripts with hand-crafted
HTML and CSS. I've whipped up PowerShell scripts. I've salvaged data
from ancient SCO Unix systems for conversion. I've reverse-engineered
complex COBOL programs and re-implemented their business logic using
modern tools.

Young hotshots think "full stack" is something to aspire to. I call it
a reasonable start.

## Here's Where I've Worked

* Deloitte Consulting -- 2015-present
* TEKsystems (on assignment to Deloitte Consulting) -- 2011-2015
* Computer Aid, Inc -- 2010-2011
* Conduit Internet Technologies -- 2009-2010
* Quality Data Service -- 2000-2009

## What I Want

I'm looking for a salary of at least $80,000 with health, dental, and
vision insurance and at least four weeks of paid leave for permanent
positions, or an hourly wage of at least $50 with guaranteed
time-and-a-half for overtime on contract jobs between three and twelve
months in duration.

I'm based in central Pennsylvania. I can work on-site, but I prefer
remote positions.

## What I Don't Want

Do not contact me about projects related to defense, law enforcement,
surveillance, or advertising.

## Availability

I am *not* available for new projects at the moment. Please do not
contact me.
